// Copyright (c) 2017 Franka Emika GmbH0
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
#include <torque_qp/robot/panda_control.h>
#include <cmath>
#include <memory>
#include <chrono> 
#include <controller_interface/controller_base.h>
#include <franka/robot_state.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>


using namespace std;

namespace torque_qp{


bool PandaController::init(hardware_interface::RobotHW* robot_hardware, ros::NodeHandle& node_handle) {    

    //--------------------------------------
    // LOAD ROBOT
    //--------------------------------------
    this->node_handle = node_handle;
    string arm_id;
    if (!node_handle.getParam("arm_id", arm_id)) {
        ROS_ERROR_STREAM("CartesianImpedanceExampleController: Could not read parameter arm_id");
        return false;
    }   
    if (!node_handle.getParam("control_level", control_level)) {
        ROS_ERROR_STREAM("Could not read parameter control_level");
        return false;
    }   
    
    std::vector<std::string> joint_names;
    if (!node_handle.getParam("joint_names", joint_names)) {
        ROS_ERROR("JointVelocityExampleController: Could not parse joint names");
    }
    if (joint_names.size() != 7) {
        ROS_ERROR_STREAM("JointVelocityExampleController: Wrong number of joint names, got "
        << joint_names.size() << " instead of 7 names!");
        return false;
    }
   
    auto state_interface = robot_hardware->get<franka_hw::FrankaStateInterface>();
    if (state_interface == nullptr) {
        ROS_ERROR("JointVelocityExampleController: Could not get state interface from hardware");
        return false;
    }
    try {
        state_handle = std::make_unique<franka_hw::FrankaStateHandle>( state_interface->getHandle("panda_robot"));
    } catch (const hardware_interface::HardwareInterfaceException& e) {
        ROS_ERROR_STREAM(
            "JointVelocityExampleController: Exception getting state handle: " << e.what());
            return false;
    }

    auto* effort_joint_interface = robot_hardware->get<hardware_interface::EffortJointInterface>();
    if (effort_joint_interface == nullptr) {
        ROS_ERROR_STREAM(
            "Error getting effort joint interface from hardware");
        return false;
    }
    for (size_t i = 0; i < 7; ++i) {
        try {
        joint_handles.push_back(effort_joint_interface->getHandle(joint_names[i]));
        } catch (const hardware_interface::HardwareInterfaceException& ex) {
        ROS_ERROR_STREAM(
            "Exception getting joint handles: " << ex.what());
        return false;
        }
    }
    
    return true;
}

void PandaController::starting(const ros::Time&)
{
    // ROS_WARN_STREAM("Starting QP Controller on the real Panda");
    Eigen::Matrix<double,7,1> q_init, qdot_init;

    franka::RobotState robot_state = state_handle->getRobotState(); 
    //Get robot current state
    for (int i =0 ; i<7 ; i++)
    {
        q_init(i) = robot_state.q[i];
        qdot_init(i) = robot_state.dq[i];
    }
    if (!initialized) // franka_control starts twice but the first one doesn't initialize the qp ...
    {
        initialized = qp.init(node_handle, q_init, qdot_init);
    }
}


void PandaController::update(const ros::Time&, const ros::Duration& period) {    

    //--------------------------------------
    // ROBOT STATE
    //--------------------------------------
    // get state variables
    franka::RobotState robot_state = state_handle->getRobotState(); 

    //Get robot current state
    for (int i =0 ; i<7 ; i++)
    {
        q(i) = robot_state.q[i];
        qdot(i) = robot_state.dq[i];
        tau_J_d(i) = robot_state.tau_J_d[i];
    }
    
    ROS_DEBUG_STREAM("Updating qp");
    joint_command = qp.update(q,qdot,tau_J_d,period);
    ROS_DEBUG_STREAM("qp updated");
    for (size_t i = 0; i < 7; ++i)
    {
        joint_handles[i].setCommand(joint_command(i));
    }    
}

}  // namespace torque_qp

PLUGINLIB_EXPORT_CLASS(torque_qp::PandaController,
controller_interface::ControllerBase)
