#!/usr/bin/env python3

import sys
import rospy
import rospkg
from panda_traj.srv import UpdateTrajectory, UpdateTrajectoryRequest


rospack = rospkg.RosPack()

def UpdateMyTrajectory(csv, is_path,verbose):
    rospy.wait_for_service('/panda/torque_qp/updateTrajectory')
    try:
        client = rospy.ServiceProxy('/panda/torque_qp/updateTrajectory', UpdateTrajectory)

        req = UpdateTrajectoryRequest();
        req.csv = csv
        req.is_path = is_path
        req.verbose = verbose
        client.call(req);
    except rospy.ServiceException as e:
        print ("Service call failed: ",e)

def usage():
    return "%s [traj_path verbose = (default false)]"%sys.argv[0]

if __name__ == "__main__":
    if len(sys.argv) == 2:
        csv = rospack.get_path('panda_traj') + "/trajectories/" + str(sys.argv[1]) + ".csv"
        is_path= True
        verbose = False
    elif len(sys.argv) == 3:
        csv = rospack.get_path('panda_traj') + "/trajectories/" + str(sys.argv[1]) + ".csv"
        is_path=  True
        verbose = False
    elif len(sys.argv) == 4:
        csv = rospack.get_path('panda_traj') + "/trajectories/" + str(sys.argv[1])+".csv"
        is_path= sys.argv[2]
        verbose = bool(sys.argv[3])
    else:
        print(usage())
        sys.exit(1)
    
    print ("Requesting ",csv)
    UpdateMyTrajectory(csv,is_path, verbose)
